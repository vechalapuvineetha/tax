package com.emp.tax.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.emp.tax.dto.EmployeeDataDto;
import com.emp.tax.service.EmpTaxService;

@RestController
public class EmpTaxController {

	@Autowired
	private EmpTaxService empTaxService;
	
	@PostMapping("/storeEmpDetails")
	public @ResponseBody String storeEmployeeDetails(@RequestBody EmployeeDataDto employeeDataDto){
		String response = empTaxService.storeEmployeeDetails(employeeDataDto);
		return response;
	}
	
	@GetMapping("/fetchAllEmpData")
	public @ResponseBody List<EmployeeDataDto> fetchAllEmployeeDetails() {
		List<EmployeeDataDto> employeeDataDtos = empTaxService.fetchAllEmployeeDetails();
		return employeeDataDtos;
	}
	
}
