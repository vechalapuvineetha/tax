package com.emp.tax.entity;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class EmployeeData {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long empID;
	private String firstName;
	private String lastName;
	private String emailId;
	private String phNum;
	private String altPhNum;
	private Date doj;
	private Double salary;

	public Long getEmpID() {
		return empID;
	}

	public void setEmpID(Long empID) {
		this.empID = empID;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPhNum() {
		return phNum;
	}

	public void setPhNum(String phNum) {
		this.phNum = phNum;
	}

	public String getAltPhNum() {
		return altPhNum;
	}

	public void setAltPhNum(String altPhNum) {
		this.altPhNum = altPhNum;
	}

	public Date getDoj() {
		return doj;
	}

	public void setDoj(Date doj) {
		this.doj = doj;
	}

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}

	@Override
	public String toString() {
		return "EmployeeData [empID=" + empID + ", firstName=" + firstName + ", lastName=" + lastName + ", emailId="
				+ emailId + ", phNum=" + phNum + ", altPhNum=" + altPhNum + ", doj=" + doj + ", salary=" + salary + "]";
	}

}
