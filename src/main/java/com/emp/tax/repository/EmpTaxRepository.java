package com.emp.tax.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.emp.tax.entity.EmployeeData;

public interface EmpTaxRepository extends JpaRepository<EmployeeData, Long>{
	
}
