package com.emp.tax.service;

import java.util.List;

import com.emp.tax.dto.EmployeeDataDto;

public interface EmpTaxService {

	String storeEmployeeDetails(EmployeeDataDto employeeDataDto);

	List<EmployeeDataDto> fetchAllEmployeeDetails();

}
