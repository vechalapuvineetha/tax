package com.emp.tax.service.impl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.emp.tax.dto.EmployeeDataDto;
import com.emp.tax.entity.EmployeeData;
import com.emp.tax.repository.EmpTaxRepository;
import com.emp.tax.service.EmpTaxService;

@Service
public class EmpTaxServiceImpl implements EmpTaxService {

	@Autowired
	private EmpTaxRepository empTaxRepository;
	
	@Override
	public String storeEmployeeDetails(EmployeeDataDto employeeDataDto) {
		String response = null;
		try {
			EmployeeData employeeData = new EmployeeData();
			employeeData.setAltPhNum(employeeDataDto.getAltPhNumber());
			employeeData.setDoj(Date.valueOf(employeeDataDto.getDoj()));
			employeeData.setEmailId(employeeDataDto.getEmailId());
			employeeData.setEmpID(employeeDataDto.getEmpID());
			employeeData.setFirstName(employeeDataDto.getFirstName());
			employeeData.setLastName(employeeDataDto.getLastName());
			employeeData.setPhNum(employeeDataDto.getPhNumber());
			employeeData.setSalary(employeeDataDto.getSalary());
			empTaxRepository.save(employeeData);
			
			response = "Saved Successfully";
		} catch (Exception e) {
			e.printStackTrace();
			response = "Failed to Save";
		}
		return response;

	}

	@Override
	public List<EmployeeDataDto> fetchAllEmployeeDetails() {
		List<EmployeeDataDto> list = null;
		try {
			
			List<EmployeeData> employeeDataList = empTaxRepository.findAll();
			list = new ArrayList<EmployeeDataDto>();
			for (EmployeeData employeeData : employeeDataList) {
				EmployeeDataDto employeeDataDto = new EmployeeDataDto();
				employeeDataDto.setEmpID(employeeData.getEmpID());
				employeeDataDto.setFirstName(employeeData.getFirstName());
				employeeDataDto.setLastName(employeeData.getLastName());
				employeeDataDto.setEmailId(employeeData.getEmailId());
				employeeDataDto.setPhNumber(employeeData.getPhNum());
				employeeDataDto.setAltPhNumber(employeeData.getAltPhNum());
				employeeDataDto.setDoj(String.valueOf(employeeData.getDoj()));
				employeeDataDto.setSalary(employeeData.getSalary());
				list.add(employeeDataDto);
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

}