package com.emp.tax.dto;

public class EmployeeDataDto {

	private Long empID;
	private String firstName;
	private String lastName;
	private String emailId;
	private String phNumber;
	private String altPhNumber;
	private String doj;
	private Double salary;

	public Long getEmpID() {
		return empID;
	}

	public void setEmpID(Long empID) {
		this.empID = empID;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPhNumber() {
		return phNumber;
	}

	public void setPhNumber(String phNumber) {
		this.phNumber = phNumber;
	}

	public String getAltPhNumber() {
		return altPhNumber;
	}

	public void setAltPhNumber(String altPhNumber) {
		this.altPhNumber = altPhNumber;
	}

	public String getDoj() {
		return doj;
	}

	public void setDoj(String doj) {
		this.doj = doj;
	}

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}

	@Override
	public String toString() {
		return "EmployeeDataDto [empID=" + empID + ", firstName=" + firstName + ", lastName=" + lastName + ", emailId="
				+ emailId + ", phNumber=" + phNumber + ", altPhNumber=" + altPhNumber + ", doj=" + doj + ", salary="
				+ salary + "]";
	}

}
